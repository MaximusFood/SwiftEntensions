//
//  SpriteKitSwiftExtensions.swift
//  v1.2.0
//
//  Created by Maxim Arnold on 19/09/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//
//  Just a bunch of common extensions that helps me code nice and quickly
//  in SpriteKit
//  


import SpriteKit


extension SKLabelNode {
    /*
    *   Changes the label text, but grows it a bit before changing, then shrinks
    *   back. Animation times are hard coded. This is bad to have as an extension,
    *   but I use it a lot. 
    */
    func animatedChangeLabel(toString newString: String) {
        // Increasing the score invloves first slightly enlargening the label...
        let action1 = SKAction.scaleTo(1.4, duration: 0.1)
        
        // ...then changing the label text to the new value...
        let action2 = SKAction.runBlock({ self.text = newString })
        
        // ...and finally, changing the label size back.
        let action3 = SKAction.scaleTo(1, duration: 0.1)
        
        self.runAction(SKAction.sequence([action1, action2, action3]))
    }
}


/*
*   ScreenPosition enum is used in some of my SKNode quick-animation etensions.
*   By extending Int, we can create a ScreenPosition from an Int, using the
*   rawValue initialiser.
*/
enum ScreenPosition: Int {
    case Bottom = 0, Top, Left, Right
}


extension SKNode {
    /*
    *   MARK: SKNode Quick Animations
    *
    *   I used to use these in Cocos2D. They were easy there as you could have 'normalised'
    *   positions, with 0 - 1 being the values. Here however, we need to know the size of
    *   the scene, so we can't do it nicely. My solution (these things are so useful I had
    *   to find a way) is to set the contentSize on Int itself when the app starts up:
    */

    /*
    *   CURRENTLY A COMPILER BUG PREVENTS ME FROM DOING THIS ON INT DIRECTLY. HACK FOLLOWS.
    *   static var contentSize: CGSize = CGPointZero
    */
    struct Hack {
        static var contentSize: CGSize = CGSizeZero
    }
    
    /*
    *   Node position is changed to off-screen, and the node flies into position. Good for
    *   when entering a scene.
    */
    func flyIn(withDuration duration: NSTimeInterval,
        delay: NSTimeInterval = 0.0,
        direction: ScreenPosition = [.Top, .Bottom, .Left, .Right].random) // default is a random position.
    {
        
        // first cache the current pos
        let dest = self.position
        
        // then we're gonna start the pice off the screen, depending on the direction par. This is currently all
        // assuming that the node has a 0.5,0.5 anchor point. SKNodes don't even have anchor points, so this is
        // wrong, but it works for me right now. Well, does frame work for labels? probably not. Eurgh I might have
        // to image up the title. cock.
        switch direction {
        case .Bottom :
            self.position = CGPoint(x: dest.x, y: -(self.frame.size.height/2))
        case .Top :
            self.position = CGPoint(x: dest.x, y: SKNode.Hack.contentSize.height + self.frame.size.height/2)
        case .Left :
            self.position = CGPoint(x: -(self.frame.size.width/2), y: dest.y)
        case .Right :
            self.position = CGPoint(x: SKNode.Hack.contentSize.width + self.frame.size.width/2, y: dest.y)
        }
        
        // then run the action
        let delayAction = SKAction.waitForDuration(delay)
        let moveAction = SKAction.moveTo(dest, duration: duration)
        moveAction.timingMode = SKActionTimingMode.EaseOut
        
        self.runAction(SKAction.sequence([delayAction, moveAction]))
    }
    
    /*
    *   Opposit to above. Good for leaving a scene.
    */
    func flyOut(withDuration duration: NSTimeInterval,
        delay: NSTimeInterval = 0.0,
        direction: ScreenPosition = [.Top, .Bottom, .Left, .Right].random) // default is a random position.
    {
        var dest = CGPointZero
        
        switch direction {
        case .Bottom :
            dest = CGPoint(x: self.position.x, y: -(self.frame.size.height/2))
        case .Top :
            dest = CGPoint(x: self.position.x, y: SKNode.Hack.contentSize.height + self.frame.size.height/2)
        case .Left :
            dest = CGPoint(x: -(self.frame.size.width/2), y: self.position.y)
        case .Right :
            dest = CGPoint(x: SKNode.Hack.contentSize.width + self.frame.size.width/2, y: self.position.y)
        }
        
        // then run the action
        let delayAction = SKAction.waitForDuration(delay)
        let moveAction = SKAction.moveTo(dest, duration: duration)
        moveAction.timingMode = SKActionTimingMode.EaseIn
        
        self.runAction(SKAction.sequence([delayAction, moveAction, SKAction.removeFromParent()]))
    }
    
    /*
    *   After a passed delay, fade in the node
    */
    func fadeIn(withDuration duration: NSTimeInterval, delay: NSTimeInterval = 0.0) {
        // First, make us invisible
        self.alpha = 0
        
        let delayAction = SKAction.waitForDuration(delay)
        let fadeAction = SKAction.fadeInWithDuration(duration)
        
        self.runAction(SKAction.sequence([delayAction, fadeAction]))
    }
    
    /*
    *   Fade out the node, and cleanup
    */
    func fadeOut(withDuration duration: NSTimeInterval, delay: NSTimeInterval = 0.0) {
        let delayAction = SKAction.waitForDuration(delay)
        let fadeAction = SKAction.fadeOutWithDuration(duration)
        
        self.runAction(SKAction.sequence([delayAction, fadeAction, SKAction.removeFromParent()]))
    }
    
    
    /*
    *   After passed delay, grow in the node. This is much better with an elastic ease
    *   action, but bloomin' SpriteKit doesn't support these yet. You can set an easing
    *   function, but the value can't go beyond 0 - 1, so no elastic. In the future it'll
    *   surely change, but for now lets leave it.
    */
    func growIn(withDuration duration: NSTimeInterval, delay: NSTimeInterval = 0.0) {
        // Set scale to 0
        self.setScale(0)
        
        let delayAction = SKAction.waitForDuration(delay)
        let scaleAction = SKAction.scaleTo(1, duration: duration)
        scaleAction.timingMode = SKActionTimingMode.EaseIn
        
        self.runAction(SKAction.sequence([delayAction, scaleAction]))
    }
    
    /*
    *   Compliment to the above. Remove from parent at the end.
    */
    func growOut(withDuration duration: NSTimeInterval, delay: NSTimeInterval = 0.0) {
        let delayAction = SKAction.waitForDuration(delay)
        let scaleAction = SKAction.scaleTo(0, duration: duration)
        scaleAction.timingMode = SKActionTimingMode.EaseIn
        
        self.runAction(SKAction.sequence([delayAction, scaleAction, SKAction.removeFromParent()]))
    }
    
    
    /*
    *   Useful to easily chuck a delay before running a block
    */
    func runBlock(block: (() -> Void), delay: NSTimeInterval = 0.0) {
        self.runAction(SKAction.waitForDuration(delay), completion: block)
    }
}

