//
//  SwiftExtensions.swift
//  v1.0.1
//
//  Created by Maxim Arnold on 19/09/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//
//  Just a bunch of common extensions that helps me code nice and quickly.
//  


extension Bool {
    mutating func flip() -> Bool {
        self = !self
        return self == true
    }
}


extension Int {

    // Returns a random integer in the range provided. 
    static func random(range: Range<Int>) -> Int
    {
        var offset = 0
        
        if range.startIndex < 0   // allow negative ranges
        {
            offset = abs(range.startIndex)
        }
        
        let mini = UInt32(range.startIndex + offset)
        let maxi = UInt32(range.endIndex   + offset)
        
        return Int(mini + arc4random_uniform(maxi - mini)) - offset
    }
    
    // Returns self, rounded to the nearest passed number. E.g.
    // 77.roundToNearest(8) == 80.
    func roundToNearest(num: Int) -> Int {
        let r = self % num
        let q = self / num
        
        if r < num/2 {
            return q * num
        } else {
            return (q + 1) * num
        }
    }

    func upperLimit(limit: Int) -> Int {
        if self > limit {
            return limit
        } else {
            return self
        }
    }
    
    func lowerLimit(limit: Int) -> Int {
        if self < limit {
            return limit
        } else {
            return self
        }
    }
}


extension Double {
    
    func upperLimit(limit: Double) -> Double {
        if self > limit {
            return limit
        } else {
            return self
        }
    }
    
    func lowerLimit(limit: Double) -> Double {
        if self < limit {
            return limit
        } else {
            return self
        }
    }
}


extension Array {
    // Calculated var on Array. Chooses a random element from the
    // array. 
    var random: Element {
        return self[Int.random(0...self.count-1)]
    }
}
