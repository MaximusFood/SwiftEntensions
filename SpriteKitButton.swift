//
//  SpriteKitButton.swift
//  v2.0.1
//
//
//  Created by Maxim Arnold on 30/10/2015.
//  Copyright © 2015 Maxim. All rights reserved.
//
//
//  Couple of lightweight spritekit buttons: MXImageButton and MXTextButton.
//  I've tried to do this as I would've in c++, but I can't make MXButtonBase abstract,
//  and there's no protected, so it's a bit off. Gonna rewrite it later w/ delegates and
//  all that Apple style stuff.
//

import SpriteKit

/*
*   The base class has all the touch handling code. What happens when touched and stuff
*   is handled in subclasses.
*/
class MXButtonBase: SKNode {
    
    var action: () -> Void // action can be changed at any time
    
    private var isBeingTouched = false
    
    
    /*
    *   MARK: Init
    */
    
    /*
    *   I've made this private which kind of makes it abstract. As there's no protected
    *   this means all the subclasses have to be in this file. Stoopid.
    */
    private init(buttonAction: () -> Void = {}) {
        action = buttonAction
        
        super.init()
        
        userInteractionEnabled = true
    }
    
    /*
    *   Required to shut XCode up
    */
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
    *   MARK: User Interaction Methods
    */
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isButtonTouched(touches) {
            buttonWasTouched()
            isBeingTouched = true
        }
    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if !isBeingTouched && isButtonTouched(touches) {
            buttonWasTouched()
            isBeingTouched = true
        } else if isBeingTouched && !isButtonTouched(touches) {
            buttonWasUntouched()
            isBeingTouched = false
        }
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        buttonWasUntouched()
        
        // if the 'touch up' is inside the button, fire off the action
        if isButtonTouched(touches) {
            action()
        }
    }
    
    
    /*
    *   MARK: Methods to be Overridden
    */
    
    private func isButtonTouched(touches: Set<UITouch>) -> Bool {
        fatalError("Method must be implemented in subclass")
    }
    
    private func buttonWasTouched() {
        fatalError("Method must be implemented in subclass")
    }
    
    private func buttonWasUntouched() {
        fatalError("Method must be implemented in subclass")
    }
}


/*
*   MARK: Derived Buttons
*/

/*
*   MXImageButton is a button with an image, that shrinks on touch.
*/
class MXImageButton: MXButtonBase {
    private(set) var buttonImage: SKSpriteNode // image can't
    
    // Use the frame of the button image. This is necessary to get a button to work w/ my
    // 'flyIn/Out' methods.
    override var frame: CGRect {
        return buttonImage.frame
    }
    
    
    /*
    *   Init
    */
    
    init(buttonImage: String, buttonAction: () -> Void = {}) {
        self.buttonImage = SKSpriteNode(imageNamed: buttonImage)
        
        super.init(buttonAction: buttonAction)
        
        addChild(self.buttonImage)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
    *   Overridden methods
    */
    
    override private func isButtonTouched(touches: Set<UITouch>) -> Bool {
        for touch in touches {
            if buttonImage.containsPoint(touch.locationInNode(self)) {
                return true
            }
        }
        return false
    }
    
    override private func buttonWasTouched() {
        self.runAction(SKAction.scaleBy(0.8, duration: 0.1))
    }
    
    override private func buttonWasUntouched() {
        self.runAction(SKAction.scaleTo(1, duration: 0.1))
    }
}


/*
*   MXTextButton has some text, and a highlight colour.
*/
class MXTextButton: MXButtonBase {
    private(set) var buttonLabel: SKLabelNode
    
    var buttonColour = SKColor.whiteColor()
    var highlightColour = SKColor.grayColor()
    
    // Use the frame of the button image. This is necessary to get a button to work w/ my
    // 'flyIn/Out' methods.
    override var frame: CGRect {
        return buttonLabel.frame
    }
    
    
    /*
    *   Init
    */
    
    init(buttonText: String,
        buttonAction: () -> Void = {}) {
            
        self.buttonLabel = SKLabelNode(text: buttonText)
        self.buttonLabel.horizontalAlignmentMode = .Center
        self.buttonLabel.verticalAlignmentMode = .Center
            
        super.init(buttonAction: buttonAction)
        
        addChild(self.buttonLabel)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    /*
    *   Overridden methods
    */
    
    override private func isButtonTouched(touches: Set<UITouch>) -> Bool {
        for touch in touches {
            if buttonLabel.containsPoint(touch.locationInNode(self)) {
                return true
            }
        }
        return false
    }
    
    override private func buttonWasTouched() {
        // First cache the colour. The user has access to the SKLabelNode, so could've
        // done all manner of things to it. 
        buttonColour = buttonLabel.fontColor!
        self.buttonLabel.fontColor = highlightColour
    }
    
    override private func buttonWasUntouched() {
        self.buttonLabel.fontColor = buttonColour
    }
}
