# Max's Lovely Swift Extensions

Just a little collection of little useful Swift class extensions I've written. They're mostly rather rough-and-ready, but feel free to put them to use. Just be careful.

I've also included a lightweight Swift SpriteKit button class. Very useful.